<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\Product;
use App\Models\Store;
use App\Http\Resources\ProductCollection;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     * @queryParam store_id string required the Store id.
     */
    public function index(Request $request)
    {
        $storeId = $request->input('store_id') ?? '';
        $query = Product::query()
            ->leftJoin('stores', 'stores.id', '=', 'products.store_id')
            ->where([
                'stores.user_id' => auth()->id(),
                'store_id' => $storeId
            ]);
        if ($request->has('search')) {
            $query->where('products.name', 'like', '%' . $request->input('search') . '%');
        }

        $limit = $request->input('limit') ?? 10;
        $page = $request->input('page') ?? 1;
        $results = $query->orderBy('products.created_at', 'DESC')
            ->select(['products.*'])
            ->paginate($limit, ['*'], 'page', $page);

        return response()->json([
            'data' => new ProductCollection($results),
            'pagination' => [
                "current_page" => $results->currentPage(),
                "first_page_url" => $results->getOptions()['path'] . '?' . $results->getOptions()['pageName'] . '=1',
                "prev_page_url" => $results->previousPageUrl(),
                "next_page_url" => $results->nextPageUrl(),
                "last_page_url" => $results->getOptions()['path'] . '?' . $results->getOptions()['pageName'] . '=' . $results->lastPage(),
                "last_page" => $results->lastPage(),
                "per_page" => $results->perPage(),
                "total" => $results->total(),
                'limit' => $results->count(),
                "path" => $results->getOptions()['path'],
            ],
            'status' => true,
            'message' => 'successful'
        ], 201);
    }

    /**
     * Product a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'price' => 'required|numeric',
            'description' => 'required|string',
            'store_id' => 'required|exists:stores,id',
        ]);
        $store = Store::findOrFail($request->input('store_id'));
        if ($store->user_id !== auth()->id()) {
            return response()->json(['message' => 'Forbidden'], 403);
        }

        $product = Product::create($request->all());
        Log::info("Product ID {$product->id} created successfully.");

        return response()->json($product, 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $product = Product::find($id);
        if (empty($product)) {
            return response()->json(['message' => 'Product not found'], 404);
        }
        if ($product->store->user_id !== auth()->id()) {
            return response()->json(['message' => 'Forbidden'], 403);
        }

        return response()->json($product);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'name' => 'sometimes|required',
            'price' => 'sometimes|required|numeric',
            'description' => 'required',
        ]);

        $product = Product::find($id);
        if (empty($product)) {
            return response()->json(['message' => 'Product not found'], 404);
        }
        if ($product->store->user_id !== auth()->id()) {
            return response()->json(['message' => 'Forbidden'], 403);
        }

        $product = Product::findOrFail($id);
        $product->update($request->all());
        Log::info("Product ID {$product->id} update successfully.");

        return response()->json($product, 200);

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {

        $product = Product::find($id);
        if (empty($product)) {
            return response()->json(['message' => 'Product not found'], 404);
        }
        if ($product->store->user_id !== auth()->id()) {
            return response()->json(['message' => 'Forbidden'], 403);
        }

        $product->delete();
        Log::info("Product ID {$product->id} delete successfully.");

        return response()->json(['message' => 'Product deleted successfully'], 204);
    }

}