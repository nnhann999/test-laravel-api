<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\User;
use App\Models\Store;
use App\Http\Resources\StoreCollection;

class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {

        $query = Store::query()->where('user_id', auth()->id());

        if ($request->has('search')) {
            $query->where('name', 'like', '%' . $request->input('search') . '%');
        }
        $limit = $request->input('limit') ?? 10;
        $page = $request->input('page') ?? 1;
        $results = $query->with('products')
            ->orderBy('created_at', 'DESC')
            ->select(['*'])
            ->paginate($limit, ['*'], 'page', $page);

        return response()->json([
            'data' => new StoreCollection($results),
            'pagination' => [
                "current_page" => $results->currentPage(),
                "first_page_url" => $results->getOptions()['path'] . '?' . $results->getOptions()['pageName'] . '=1',
                "prev_page_url" => $results->previousPageUrl(),
                "next_page_url" => $results->nextPageUrl(),
                "last_page_url" => $results->getOptions()['path'] . '?' . $results->getOptions()['pageName'] . '=' . $results->lastPage(),
                "last_page" => $results->lastPage(),
                "per_page" => $results->perPage(),
                "total" => $results->total(),
                'limit' => $results->count(),
                "path" => $results->getOptions()['path'],
            ],
            'status' => true,
            'message' => 'successful'
        ], 201);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'location' => 'nullable|string',
        ]);

        $store = $request->user()->stores()->create($request->all());
        Log::info("Store ID {$store->id} created successfully.");
        return response()->json($store, 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $store = Store::find($id);
        if (empty($store)) {
            return response()->json(['message' => 'Store not found'], 404);
        }
        if ($store->user_id !== auth()->id()) {
            return response()->json(['message' => 'Forbidden'], 403);
        }

        return response()->json($store);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $store = Store::find($id);
        if (empty($store)) {
            return response()->json(['message' => 'Store not found'], 404);
        }
        if ($store->user_id !== auth()->id()) {
            return response()->json(['message' => 'Forbidden'], 403);
        }

        $request->validate([
            'name' => 'required|string|max:255',
            'location' => 'required|string',
        ]);

        $store->update($request->all());
        Log::info("Store ID {$store->id} update successfully.");

        return response()->json($store, 200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $store = Store::find($id);
        if (empty($store)) {
            return response()->json(['message' => 'Store not found'], 404);
        }
        if ($store->user_id !== auth()->id()) {
            return response()->json(['message' => 'Forbidden'], 403);
        }

        $store->delete();
        Log::info("Store ID {$store->id} delete successfully.");

        return response()->json(['message' => 'Store deleted successfully']);
    }
}