<?php

namespace Database\Factories;

use \App\Models\User;
use App\Models\Store;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class StoreFactory extends Factory
{
    protected $model = Store::class;
    public function definition(): array
    {
        return [
            'user_id' => User::factory(),
            'name' => fake()->company,
            'location' => fake()->address,
            
        ];
    }
}
