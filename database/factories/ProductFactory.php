<?php

namespace Database\Factories;

use App\Models\Store;
use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Database\Factories\UserFactory;

class ProductFactory extends Factory
{
    protected $model = Product::class;

    public function definition()
    {
        return [
            'store_id' => Store::factory(),
            'name' => fake()->word,
            'price' => fake()->randomFloat(2, 1, 100),
            'description' => fake()->paragraph,
        ];
    }
}
