<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// user login
Route::post('/login', [AuthController::class, 'login']);
Route::middleware(['auth:sanctum'])->group(function () {
    // user logout
    Route::post('/logout', [AuthController::class, 'logout']);
    // store
    Route::group([
        'prefix' => '/store',
        'namespace' => 'App\Http\Controllers',
    ], function () {
        Route::get('/list', 'StoreController@index');
        Route::get('/show/{id}', 'StoreController@show');
        Route::put('/edit/{id}', 'StoreController@update');
        Route::post('/create', 'StoreController@store');
        Route::delete('/delete/{id}', 'StoreController@destroy');
    });
    // product
    Route::group([
        'prefix' => '/product',
        'namespace' => 'App\Http\Controllers',
    ], function () {
        Route::get('/list', 'ProductController@index');
        Route::get('/show/{id}', 'ProductController@show');
        Route::put('/edit/{id}', 'ProductController@update');
        Route::post('/create', 'ProductController@store');
        Route::delete('/delete/{id}', 'ProductController@destroy');
    });

});